<?php
require __DIR__ . '/vendor/autoload.php';
$complexString1 = '5−6i';
$complexString2 = '−3+2i';
$real = 5;
$imaginary = -6;
$suffix = 'i';
$args1 = [$real, $imaginary, $suffix];

$real = -3;
$imaginary = 2;
$suffix = 'i';
$args2 = [$real, $imaginary, $suffix];
$complexObject = new Complex\Complex($args1);
?>
<p>вычитание</p>
<p><?=$complexObject->subtract($args2)?></p>
<p>сложение</p>
<p><?=$complexObject->add($args2)?></p>
<p>умножение</p>
<p><?=$complexObject->multiply($args2)?></p>
<p>деление</p>
<p><?=$complexObject->divideby($args2)?></p>