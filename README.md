**Установка**  
php 7.4

В корневом каталоге выполнить
```
git clone https://Eugeny-Ivanov@bitbucket.org/Eugeny-Ivanov/laptop-test.git .
```
```
composer install
```

При отсутсвии composer-а в файле /laptop-test.zip собранное
приложение. Распаковать в корень. Должно завестись сразу.